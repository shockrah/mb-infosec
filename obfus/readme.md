# Obfuscation Challenges

Not all challenges may be in the same language, some may use C principles while others are more focused around C++, Javascript, assembly etc.

## Target Platform 

The only caveat is that, for the more _low level_ challenges assume the target platform to be x86\_64 Linux unless specified otherwise.
This means any challanges that happen to be written in assembly and some C/C++ challenges

## Disclaimer

Obfuscated code is __not__ a good way of securing your programs. 
There are much more effective ways of going about ensuring that your programs are secure.

That being said this section is more or less here for _teh lulz_, or because some of the material is just interesting to read through.
